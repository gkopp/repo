# HCAL RPM Repository

This repo contains the tagged binary and source RPMs from the [HCAL XDAQ Repository](https://gitlab.cern.ch/cmshcos/hcal).

### Usage: `git clone https://gitlab.cern.ch/cmshcos/repo.git repo`

This will checkout the latest stable release in a repository format under `repo/`

##### Additionally, you can check out any tagged release by executing:

### `git clone https://gitlab.cern.ch/cmshcos/repo.git --branch 14_0_0 --single-branch`
which will checkout release 14_0_0 in repository format.

### Installing the releases
You are free to install as root:

`yum install repo/*.rpm`

or as unprivileged/teststand:

`python2 installDAQ.py`
